'use strict'

import register from './lib/register'
import login from './lib/login'
import loginByWeixin from './lib/login-by-weixin'
import bindWeixin from './lib/bind-weixin'
import unbindWeixin from './lib/unbind-weixin'
import loginByAlipay from './lib/login-by-alipay'
import bindAlipay from './lib/bind-alipay'
import unbindAlipay from './lib/unbind-alipay'
import logout from './lib/logout'
import updatePwd from './lib/update-pwd'
import updateUser from './lib/update-user'
// import deleteUser from './deleteUser'
import setAvatar from './lib/set-avatar'
import bindMobile from './lib/bind-mobile'
import bindEmail from './lib/bind-email'
import uniToken from './lib/uni-token'
import encryptPwd from './lib/encrypt-pwd'
import resetPwd from './lib/reset-pwd'
import {
  setVerifyCode,
  verifyCode
} from './lib/verify'
import unbindMobile from './lib/unbind-mobile'
import sendSmsCode from './lib/send-sms-code'
import loginBySms from './lib/login-by-sms'
import loginByEmail from './lib/login-by-email'
import unbindEmail from './lib/unbind-email'
import setUserInviteCode from './lib/set-user-invite-code'
import acceptInvite from './lib/accept-invite'
import getInvitedUser from './lib/get-invited-user'
import getUserInfo from './lib/get-user-info'
import {
  init
} from './share/index'

const checkToken = uniToken.checkToken

export default {
  init,
  register,
  login,
  loginByWeixin,
  bindWeixin,
  unbindWeixin,
  loginByAlipay,
  bindAlipay,
  unbindAlipay,
  logout,
  updatePwd,
  updateUser,
  setAvatar,
  bindMobile,
  bindEmail,
  checkToken,
  encryptPwd,
  resetPwd,
  unbindMobile,
  setVerifyCode,
  verifyCode,
  sendSmsCode,
  loginBySms,
  loginByEmail,
  unbindEmail,
  setUserInviteCode,
  acceptInvite,
  getUserInfo,
  getInvitedUser
}
