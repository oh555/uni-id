import uniToken from './uni-token'
import {
  userCollection,
  log
} from '../share/index'
import getWeixinApi from '../common/weixin-api'
import loginCheck from '../common/login-check'
import registerExec from '../common/register-exec'

const db = uniCloud.database()
async function loginByWeixin (code) {
  let params = code
  if (typeof code === 'string') {
    params = {
      code
    }
  }
  const clientPlatform = params.platform || __ctx__.PLATFORM
  const {
    openid,
    unionid
  } = await getWeixinApi({
    platform: clientPlatform
  })[clientPlatform === 'mp-weixin' ? 'code2Session' : 'getOauthAccessToken'](params.code)
  if (!openid) {
    return {
      code: 10401,
      msg: '获取openid失败'
    }
  }
  const dbCmd = db.command
  const queryUser = [{
    wx_openid: {
      [clientPlatform]: openid
    }
  }]
  if (unionid) {
    queryUser.push({
      wx_unionid: unionid
    })
  }
  const userList = await userCollection.where(dbCmd.or(...queryUser)).get()
  // openid 或 unionid已注册
  if (userList && userList.data && userList.data.length > 0) {
    const userMatched = userList.data[0]
    try {
      const loginCheckRes = await loginCheck(userMatched)
      if (loginCheckRes.code !== 0) {
        return loginCheckRes
      }
      const userInfo = loginCheckRes.user
      const tokenList = userInfo.token

      log('开始修改最后登录时间，写入unionid（可能不存在）和openid')
      const {
        token,
        tokenExpired
      } = uniToken.createToken(userMatched)
      log('token', token)
      tokenList.push(token)
      const updateData = {
        last_login_date: new Date().getTime(),
        last_login_ip: __ctx__.CLIENTIP,
        token: tokenList,
        wx_openid: {
          [clientPlatform]: openid
        }
      }
      if (unionid) {
        updateData.wx_unionid = unionid
      }
      const upRes = await userCollection.doc(userMatched._id).update(updateData)
      log('upRes', upRes)
      return {
        code: 0,
        msg: '登录成功',
        token,
        tokenExpired,
        uid: userMatched._id,
        username: userMatched.username,
        openid,
        unionid,
        action: 'login',
        mobileConfirmed: userInfo.mobile_confirmed === 1,
        emailConfirmed: userInfo.email_confirmed === 1
      }
    } catch (e) {
      log('写入异常：', e)
      return {
        code: 90001,
        msg: '数据库写入异常'
      }
    }
  } else {
    try {
      const user = {
        register_date: new Date().getTime(),
        register_ip: __ctx__.CLIENTIP,
        wx_openid: {
          [clientPlatform]: openid
        },
        wx_unionid: unionid
      }
      const myInviteCode = params.myInviteCode
      if (myInviteCode) {
        user.my_invite_code = myInviteCode
      }
      const registerResult = await registerExec(user)
      if (registerResult.code > 0) {
        return registerResult
      }
      const addRes = registerResult.result
      const uid = addRes.id
      const {
        token,
        tokenExpired
      } = uniToken.createToken({
        _id: uid
      })
      await userCollection.doc(uid).update({
        token: [token]
      })
      return {
        code: 0,
        msg: '登录成功',
        token: token,
        tokenExpired,
        uid: uid,
        openid,
        unionid,
        action: 'register',
        mobileConfirmed: false,
        emailConfirmed: false
      }
    } catch (e) {
      log('写入异常：', e)
      return {
        code: 90001,
        msg: '数据库写入异常'
      }
    }
  }
}

export default loginByWeixin
