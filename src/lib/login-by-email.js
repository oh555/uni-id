import {
  userCollection,
  log
} from '../share/index'
import uniToken from './uni-token'
import {
  verifyCode
} from './verify'
import loginCheck from '../common/login-check'
import encryptPwd from './encrypt-pwd'
import registerExec from '../common/register-exec'

async function loginByEmail ({
  email,
  code,
  password,
  myInviteCode,
  type
}) {
  const verifyRes = await verifyCode({
    email,
    code,
    type: type || 'login'
  })
  if (verifyRes.code !== 0) {
    return verifyRes // 验证失败
  }
  const query = {
    email,
    email_confirmed: 1
  }
  const userInDB = await userCollection.where(query).get()

  log('userInDB:', userInDB)

  if (userInDB && userInDB.data && userInDB.data.length > 0) {
    if (type === 'register') {
      return {
        code: 10301,
        msg: '此邮箱已注册'
      }
    }
    const userMatched = userInDB.data[0]
    try {
      const loginCheckRes = await loginCheck(userMatched)
      if (loginCheckRes.code !== 0) {
        return loginCheckRes
      }
      const tokenList = loginCheckRes.user.token

      log('开始修改最后登录时间')

      const {
        token,
        tokenExpired
      } = uniToken.createToken(userMatched)
      log('token', token)
      tokenList.push(token)
      const upRes = await userCollection.doc(userMatched._id).update({
        last_login_date: new Date().getTime(),
        last_login_ip: __ctx__.CLIENTIP,
        token: tokenList
      })

      log('upRes', upRes)

      return {
        code: 0,
        msg: '登录成功',
        token: token,
        uid: userMatched._id,
        username: userMatched.username,
        email,
        type: 'login',
        tokenExpired: tokenExpired
      }
    } catch (e) {
      log('写入异常：', e)
      return {
        code: 90001,
        msg: '数据库写入异常'
      }
    }
  } else {
    // 注册用户
    if (type === 'login') {
      return {
        code: 10302,
        msg: '此邮箱尚未注册'
      }
    }
    const user = {
      email,
      email_confirmed: 1,
      register_ip: __ctx__.CLIENTIP,
      register_date: Date.now()
    }
    if (type === 'register') {
      if (password) {
        user.password = encryptPwd(password)
      }
      if (myInviteCode) {
        user.my_invite_code = myInviteCode
      }
    }
    const registerResult = await registerExec(user)
    if (registerResult.code > 0) {
      return registerResult
    }
    const addRes = registerResult.result
    log('addRes', addRes)
    const uid = addRes.id

    if (addRes.id) {
      const {
        token,
        tokenExpired
      } = uniToken.createToken({
        _id: uid
      })
      await userCollection.doc(uid).update({
        token: [token]
      })
      return {
        code: 0,
        msg: '注册成功',
        uid,
        email,
        type: 'register',
        token,
        tokenExpired
      }
    }
    return {
      code: 90001,
      msg: '数据库写入失败'
    }
  }
}

export default loginByEmail
