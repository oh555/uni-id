import {
  userCollection,
  log
} from '../share/index'

async function updateUser (params) {
  const uid = params.uid
  if (!uid) {
    return {
      code: 80101,
      msg: '缺少uid参数'
    }
  }
  delete params.uid
  try {
    const upRes = await userCollection.doc(uid).update(params)

    log('update -> upRes', upRes)

    return {
      code: 0,
      msg: '修改成功'
    }
  } catch (e) {
    log('发生异常', e)
    return {
      code: 90001,
      msg: '数据库写入异常'
    }
  }
}

export default updateUser
