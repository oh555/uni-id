import encryptPwd from './encrypt-pwd'
import {
  userCollection,
  log
} from '../share/index'
import uniToken from './uni-token'
import registerExec from '../common/register-exec'

const db = uniCloud.database()
async function register (user) {
  const query = []
  const uniqueParam = [{
    name: 'username',
    desc: '用户名'
  }, {
    name: 'email',
    desc: '邮箱',
    extraCond: {
      email_confirmed: 1
    }
  }, {
    name: 'mobile',
    desc: '手机号',
    extraCond: {
      mobile_confirmed: 1
    }
  }]
  uniqueParam.forEach(item => {
    const paramName = item.name
    if (user[paramName] && user[paramName].trim()) {
      query.push({
        [paramName]: user[paramName],
        ...item.extraCond
      })
    }
  })

  if (query.length === 0) {
    return {
      code: 20101,
      msg: '用户名、邮箱、手机号不可同时为空'
    }
  }

  const {
    username,
    email,
    mobile,
    myInviteCode
  } = user

  const dbCmd = db.command
  try {
    const userInDB = await userCollection.where(dbCmd.or(...query)).get()

    log('userInDB:', userInDB)

    if (userInDB && userInDB.data.length > 0) {
      const userToCheck = userInDB.data[0]
      for (let i = 0; i < uniqueParam.length; i++) {
        const paramItem = uniqueParam[i]
        let extraCondMatched = true
        if (paramItem.extraCond) {
          extraCondMatched = Object.keys(paramItem.extraCond).every((key) => {
            return userToCheck[key] === paramItem.extraCond[key]
          })
        }
        if (userToCheck[paramItem.name] === user[paramItem.name] && extraCondMatched) {
          return {
            code: 20102,
            msg: `${paramItem.desc}已存在`
          }
        }
      }
    }

    user.password = encryptPwd(user.password)
    user.register_date = new Date().getTime()
    user.register_ip = __ctx__.CLIENTIP
    if (myInviteCode) {
      user.my_invite_code = myInviteCode
    }
    const registerResult = await registerExec(user)
    if (registerResult.code > 0) {
      return registerResult
    }
    const addRes = registerResult.result
    log('addRes', addRes)
    const uid = addRes.id
    const {
      token,
      tokenExpired
    } = uniToken.createToken({
      _id: uid
    })
    await userCollection.doc(uid).update({
      token: [token]
    })

    return {
      code: 0,
      uid,
      username,
      email,
      mobile,
      msg: '注册成功',
      token,
      tokenExpired
    }
  } catch (e) {
    return {
      code: 90001,
      msg: '数据库写入异常'
    }
  }
}

export default register
