import {
  userCollection
} from '../share/index'
import getValidInviteCode from '../common/get-valid-Invite-code'

export default async function ({
  uid,
  myInviteCode
}) {
  const validResult = await getValidInviteCode({
    inviteCode: myInviteCode
  })
  if (validResult.code > 0) {
    return validResult
  }
  try {
    await userCollection.doc(uid).update({
      my_invite_code: validResult.inviteCode
    })
    return {
      code: 0,
      msg: '邀请码设置成功',
      myInviteCode: validResult.inviteCode
    }
  } catch (error) {
    return {
      code: 90001,
      msg: '数据库写入异常'
    }
  }
}
