import jwt from 'jsonwebtoken'
import {
  getConfig,
  userCollection,
  log
} from '../share/index'
import crypto from 'crypto'

function getClientUaHash () {
  const hash = crypto.createHash('md5')
  const hashContent = /MicroMessenger/i.test(__ctx__.CLIENTUA) ? __ctx__.CLIENTUA.split(' Process/appbrand')[0] : __ctx__.CLIENTUA
  hash.update(hashContent)
  return hash.digest('hex')
}

const uniToken = {
  createToken: function (user) {
    const config = getConfig()
    const signContent = {
      uid: user._id
    }
    if (config.bindTokenToDevice) {
      signContent.clientId = getClientUaHash()
    }
    const token = jwt.sign(signContent, config.tokenSecret, {
      expiresIn: config.tokenExpiresIn
    })

    return {
      token,
      tokenExpired: Date.now() + config.tokenExpiresIn * 1000
    }
  },
  refreshToken: function () {
    // TODO
  },

  checkToken: async function (token) {
    const config = getConfig()
    try {
      const payload = jwt.verify(token, config.tokenSecret)
      if (config.bindTokenToDevice && payload.clientId !== getClientUaHash()) {
        return {
          code: 30201,
          msg: 'token不合法，请重新登录'
        }
      }

      const userInDB = await userCollection.doc(payload.uid).get()

      if (!userInDB.data || userInDB.data.length === 0 || !userInDB.data[0].token) {
        return {
          code: 30202,
          msg: 'token不合法，请重新登录'
        }
      }
      const userMatched = userInDB.data[0]
      if (userMatched.status === 1) {
        return {
          code: 10001,
          msg: '账号已禁用'
        }
      }
      let tokenList = userMatched.token
      if (typeof tokenList === 'string') {
        tokenList = [tokenList]
      }
      if (tokenList.indexOf(token) === -1) {
        return {
          code: 30202,
          msg: 'token不合法，请重新登录'
        }
      }

      log('checkToken payload', payload)

      return {
        code: 0,
        msg: 'token校验通过',
        ...payload,
        userInfo: userMatched
      }
    } catch (err) {
      if (err.name === 'TokenExpiredError') {
        return {
          code: 30203,
          msg: 'token已过期，请重新登录',
          err: err
        }
      }

      return {
        code: 30204,
        msg: '非法token',
        err: err
      }
    }
  },
  getExpiredToken (tokenList) {
    const config = getConfig()
    const tokenExpired = []
    tokenList.forEach(token => {
      try {
        jwt.verify(token, config.tokenSecret)
      } catch (error) {
        tokenExpired.push(token)
      }
    })
    return tokenExpired
  }
}

export default uniToken
