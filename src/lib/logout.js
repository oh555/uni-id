import {
  userCollection
} from '../share/index'
import uniToken from './uni-token'

const db = uniCloud.database()
async function logout (token) {
  const payload = await uniToken.checkToken(token)

  if (payload.code && payload.code > 0) {
    return payload
  }
  try {
    const dbCmd = db.command
    await userCollection.doc(payload.uid).update({
      token: dbCmd.pull(token)
    })

    return {
      code: 0,
      msg: '退出成功'
    }
  } catch (e) {
    return {
      code: 90001,
      msg: '数据库写入异常'
    }
  }
}

export default logout
