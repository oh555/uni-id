import { setVerifyCode } from './verify';
import { getConfig } from '../share/config';
import { userCollection } from '../share/index';
export default async function ({ mobile, code, type }) {
  if (!mobile) {
    throw new Error('手机号码不可为空');
  }
  if (!code) {
    throw new Error('验证码不可为空');
  }
  if (!type) {
    throw new Error('验证码类型不可为空');
  }
  const config = getConfig();
  let smsConfig = config && config.service && config.service.sms;
  if (!smsConfig) {
    throw new Error('请在config.json或init方法中配置service.sms下短信相关参数');
  }
  smsConfig = Object.assign(
    {
      codeExpiresIn: 300,
    },
    smsConfig,
  );
  const paramRequired = ['name', 'smsKey', 'smsSecret'];
  for (let i = 0, len = paramRequired.length; i < len; i++) {
    const paramName = paramRequired[i];
    if (!smsConfig[paramName]) {
      throw new Error(
        `请在config.json或init方法中service.sms下配置${paramName}`,
      );
    }
  }
  const { name, smsKey, smsSecret, codeExpiresIn } = smsConfig;
  let action;
  switch (type) {
    case 'login':
      action = '登录';
      break;
    default:
      action = '验证手机号';
      break;
  }
  try {
    const query = {
      mobile,
      mobile_confirmed: 1,
    };
    const userInDB = await userCollection.where(query).get();
    if (userInDB && userInDB.data && userInDB.data.length > 0) {
      await uniCloud.sendSms({
        smsKey,
        smsSecret,
        phone: mobile,
        templateId: 'uniID_code',
        data: {
          name,
          code,
          action,
          expMinute: '' + Math.round(codeExpiresIn / 60),
        },
      });
      const setCodeRes = await setVerifyCode({
        mobile,
        code,
        expiresIn: codeExpiresIn,
        type,
      });
      if (setCodeRes.code >= 0) {
        return setCodeRes;
      }
      return {
        code: 0,
        msg: '验证码发送成功',
      };
    } else {
      return {
        code: 10203,
        msg: '此手机号尚未注册',
      };
    }
  } catch (e) {
    return {
      code: 50301,
      msg: `验证码发送失败, ${e.message}`,
    };
  }
}
