import encryptPwd from './encrypt-pwd'
import {
  userCollection,
  log
} from '../share/index'

async function updatePwd (user) {
  const userInDB = await userCollection.doc(user.uid).get()

  if (userInDB && userInDB.data && userInDB.data.length > 0) {
    const pwdInDB = userInDB.data[0].password

    if (encryptPwd(user.oldPassword) === pwdInDB) { // 旧密码匹配
      try {
        const upRes = await userCollection.doc(userInDB.data[0]._id).update({
          password: encryptPwd(user.newPassword),
          token: []
        })

        log('upRes', upRes)

        return {
          code: 0,
          msg: '修改成功'
        }
      } catch (e) {
        log('发生异常', e)
        return {
          code: 90001,
          msg: '数据库写入异常'
        }
      }
    } else {
      return {
        code: 40202,
        msg: '旧密码错误'
      }
    }
  } else {
    return {
      code: 40201,
      msg: '用户不存在'
    }
  }
}

export default updatePwd
