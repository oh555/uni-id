import {
  userCollection,
  log
} from '../share/index'

const db = uniCloud.database()
async function unbindAlipay (uid) {
  try {
    const dbCmd = db.command
    const upRes = await userCollection.doc(uid).update({
      ali_openid: dbCmd.remove()
    })
    log('upRes:', upRes)
    if (upRes.updated === 1) {
      return {
        code: 0,
        msg: '支付宝解绑成功'
      }
    } else {
      return {
        code: 70401,
        msg: '支付宝解绑失败，请稍后再试'
      }
    }
  } catch (e) {
    log('写入异常：', e)
    return {
      code: 90001,
      msg: '数据库写入异常'
    }
  }
}

export default unbindAlipay
