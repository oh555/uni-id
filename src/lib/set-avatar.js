// import uniToken from './uniToken.js'
import {
  userCollection,
  log
} from '../share/index'

async function setAvatar (params) {
  try {
    const upRes = await userCollection.doc(params.uid).update({
      avatar: params.avatar
    })

    log('setAvatar -> upRes', upRes)

    return {
      code: 0,
      msg: '头像设置成功'
    }
  } catch (e) {
    log('发生异常', e)
    return {
      code: 90001,
      msg: '数据库写入异常'
    }
  }
}

export default setAvatar
