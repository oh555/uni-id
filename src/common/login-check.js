import {
  log
} from '../share/index'
import uniToken from '../lib/uni-token'

export default async function (user) {
  if (user.status === 1) {
    return {
      code: 10001,
      msg: '账号已禁用'
    }
  }

  log('过期token清理')
  let tokenList = user.token || []
  // 兼容旧版逻辑
  if (typeof tokenList === 'string') {
    tokenList = [tokenList]
  }
  const expiredToken = uniToken.getExpiredToken(tokenList)
  tokenList = tokenList.filter(item => {
    return expiredToken.indexOf(item) === -1
  })
  user.token = tokenList
  return {
    code: 0,
    user: user
  }
}
