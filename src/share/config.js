import fs from 'fs'
import path from 'path'

const db = uniCloud.database()
const userCollection = db.collection('uni-id-users')
const verifyCollection = db.collection('uni-verify')

let configFileContent = {}
try {
  configFileContent = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'config.json')))
} catch (error) {
  // 不处理错误，提供init方法
}

// 导出方法防止示例复用带来的问题
function getConfig (platform) {
  const platformConfig = Object.assign(configFileContent, configFileContent[platform || __ctx__.PLATFORM]) || {}
  const defaultConfig = {
    bindTokenToDevice: true
  }
  const config = Object.assign(defaultConfig, platformConfig)
  const argsRequired = ['passwordSecret', 'tokenSecret', 'tokenExpiresIn', 'passwordErrorLimit', 'passwordErrorRetryTime']
  argsRequired.forEach((item) => {
    if (!config || !config[item]) {
      throw new Error(`请在公用模块uni-id的config.json或init方法中内添加配置项：${item}`)
    }
  })
  return config
}

function init (config) {
  configFileContent = config
}

export {
  userCollection,
  verifyCollection,
  getConfig,
  init
}
